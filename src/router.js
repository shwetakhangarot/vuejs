import { createWebHistory, createRouter } from "vue-router";
// import home from "./components/home";
import calculator from "./components/calculator";
import quiz from "./components/quiz";


const routes = [
//   {
//     path: "/",
//     name: "home",
//     component: home,
//   },
  {
    path: "/calculator",
    name: "calculator",
    component: calculator,
  },
  {
    path: "/quiz",
    name: "quiz",
    component: quiz,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;