import { createStore } from 'vuex';
import quizData from './modules/quizData'

export default new createStore({
 
  modules: {
    quizData
  }
});
