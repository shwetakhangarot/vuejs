import axios from "axios";
const state = {
    quizData: []
}
const getters = {
    alldata: state => state.quizData
}
const actions = {
    async getData({ commit }) {
        const response = await axios.get(" https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=boolean&encode=url3986 ")
        commit("setData", response.data.results)
        // console.log(response.data.results)
    }
}
const mutations = {
    setData:(state, quizData) =>(state.quizData = quizData)
}

export default {
    state,
    getters,
    actions,
    mutations
}